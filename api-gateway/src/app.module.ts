import { MiddlewareConsumer, Module, RequestMethod } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { LoginModule } from './login/login.module';
import { KafkaModule } from './kafka/kafka.module';
import { PostsModule } from './posts/posts.module';
import { JwtMiddleware } from './auth/jwt/jwt.middleware';
import { MongodbModule } from './mongodb/mongodb.module';

const excludedPaths = [{ path: 'login', method: RequestMethod.POST }];
@Module({
  imports: [MongodbModule, LoginModule, KafkaModule, PostsModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(JwtMiddleware)
      .exclude(...excludedPaths)
      .forRoutes('*');
  }
}
