import {
  Inject,
  Injectable,
  OnApplicationShutdown,
  OnModuleInit,
} from '@nestjs/common';
import { ClientKafka } from '@nestjs/microservices';

@Injectable()
export class KafkaService implements OnModuleInit, OnApplicationShutdown {
  constructor(
    @Inject('BLOG_SERVICE') private readonly blogService: ClientKafka,
  ) {}
  async onApplicationShutdown() {
    await this.blogService.close();
  }
  async onModuleInit() {
    const requestPatterns = ['post.create', 'post.list', 'post.info'];

    requestPatterns.forEach((pattern) => {
      this.blogService.subscribeToResponseOf(pattern);
    });
    await this.blogService.connect();
  }
}
