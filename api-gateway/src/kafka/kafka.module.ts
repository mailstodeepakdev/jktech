import { Module } from '@nestjs/common';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { Partitioners } from 'kafkajs';
import { KafkaService } from './kafka.service';

@Module({
  imports: [
    ClientsModule.register([
      {
        name: 'BLOG_SERVICE',
        transport: Transport.KAFKA,
        options: {
          client: {
            //production  brokers: ['kafka:9092'],
            brokers: ['localhost:8090'],
          },
          consumer: {
            groupId: 'blog-consumer',
          },
          producer: {
            createPartitioner: Partitioners.LegacyPartitioner,
          },
        },
      },
    ]),
  ],
  exports: [ClientsModule],
  providers: [KafkaService],
})
export class KafkaModule {}
