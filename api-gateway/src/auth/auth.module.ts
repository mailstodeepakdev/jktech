import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { User, UserSchema } from '../mongodb/schema/user.schema';
import { MongooseModule } from '@nestjs/mongoose';
import { JwtService } from './jwt/jwt.service';
@Module({
  imports: [
    MongooseModule.forFeature([{ name: User.name, schema: UserSchema }]),
  ],
  providers: [AuthService, JwtService],
  exports: [AuthService, JwtService],
})
export class AuthModule {}
