import { Injectable } from '@nestjs/common';
import * as jwt from 'jsonwebtoken';
import * as bcrypt from 'bcrypt';
import { User, UserDocument } from '../mongodb/schema/user.schema';
import { InjectModel } from '@nestjs/mongoose';
import { LoginUserDto } from './dtos/login-user.dto';
import { UserDataDto } from './dtos/user-data.dto';
import { Model } from 'mongoose';

@Injectable()
export class AuthService {
  constructor(
    @InjectModel(User.name)
    private readonly userModel: Model<UserDocument>,
  ) {}
  async loginHandler(data: LoginUserDto) {
    const token = await this.tokenGenerator(data, data.secret, data.options);
    return { token: token };
  }

  async tokenGenerator(
    data: UserDataDto,
    JWTsecret: string,
    JWToptions: object,
  ) {
    const payload = {
      email: data.email,
      id: data.id,
    };
    const secret = JWTsecret;
    const options = JWToptions;

    const token = jwt.sign(payload, secret, options);
    return token;
  }
  async validatePassword(password: string, hash: string): Promise<boolean> {
    return bcrypt.compare(password, hash);
  }
  async getUserByEmailId(email: string): Promise<any> {
    const user = await this.userModel.findOne({ email, isActive: true }).lean();
    if (!user) {
      throw new Error(`User with email ${email} not found.`);
    }
    return user;
  }
}
