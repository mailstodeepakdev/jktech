import {
  HttpException,
  HttpStatus,
  Injectable,
  NestMiddleware,
} from '@nestjs/common';
import * as jwt from 'jsonwebtoken';
import { config } from '../../config/configuration';

@Injectable()
export class JwtMiddleware implements NestMiddleware {
  async use(req: any, res: any, next: () => void) {
    console.log('.....Checking Token.....');
    const formatToken = req.headers.token;
    const isToken = await this.verifyToken(formatToken);
    if (!isToken.email) {
      throw new HttpException(
        'Invalid session! Please login again',
        HttpStatus.UNAUTHORIZED,
      );
    }
    req.body.tokenData = isToken;
    next();
  }
  async verifyToken(token: string): Promise<any> {
    return jwt.verify(token, config.JWTsecret, function (err, data) {
      if (err) {
        console.log('Invalid user:' + err);
        return err;
      }
      console.log('Valid user found.');
      return data;
    });
  }
}
