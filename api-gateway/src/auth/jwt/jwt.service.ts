import { Injectable } from '@nestjs/common';
import { config } from '../../config/configuration';
import * as jwt from 'jsonwebtoken';

@Injectable()
export class JwtService {
  async getUserData(token: string): Promise<any> {
    return jwt.verify(token, config.JWTsecret, function (err: any, data: any) {
      if (err) {
        return err;
      }
      return data;
    });
  }
}
