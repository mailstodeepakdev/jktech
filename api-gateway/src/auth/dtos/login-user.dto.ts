import { IsEmail, IsNotEmpty } from 'class-validator';

export class LoginUserDto {
  @IsEmail()
  @IsNotEmpty()
  email: string;

  id: string;

  @IsNotEmpty()
  secret: string;

  @IsNotEmpty()
  options: object;
}
