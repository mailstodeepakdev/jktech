import { IsEmail, IsNotEmpty } from 'class-validator';

export class UserDataDto {
  @IsEmail()
  @IsNotEmpty()
  email: string;

  id: string;
}
