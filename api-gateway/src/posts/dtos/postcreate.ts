import { IsNotEmpty } from 'class-validator';

export interface TokenData {
  id: string;
  _id: string;
}

export class PostCreatDto {
  @IsNotEmpty()
  title: string;

  @IsNotEmpty()
  body: string;

  id: string;

  @IsNotEmpty()
  tokenData: TokenData;
}
