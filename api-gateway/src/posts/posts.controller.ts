import {
  Body,
  Controller,
  Post,
  Get,
  Res,
  HttpStatus,
  Query,
  Param,
} from '@nestjs/common';
import { PostsService } from './posts.service';
import { PostCreatDto } from './dtos/postcreate';
@Controller('posts')
export class PostsController {
  constructor(private PostsService: PostsService) {}
  @Get('')
  async getPosts(@Body() req: any, @Res() res): Promise<any> {
    try {
      const result = await this.PostsService.getPosts(req);
      res.status(HttpStatus.OK).send(result);
    } catch (err) {
      console.log(err);
      res.status(HttpStatus.OK).send(err);
    }
  }
  @Post('')
  async createPost(@Body() req: PostCreatDto, @Res() res): Promise<any> {
    try {
      const result = await this.PostsService.createPost(req);
      res.status(HttpStatus.OK).send(result);
    } catch (err) {
      console.log(err);
      res.status(HttpStatus.OK).send(err);
    }
  }
  @Get('info')
  async getPostInfo(
    @Body() req: any,
    @Query('id') id: string,
    @Res() res,
  ): Promise<any> {
    try {
      const result = await this.PostsService.getPostInfo(req, id);
      res.status(HttpStatus.OK).send(result);
    } catch (err) {
      console.log(err);
      res.status(HttpStatus.OK).send(err);
    }
  }
}
