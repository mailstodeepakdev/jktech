import { HttpStatus, Inject, Injectable } from '@nestjs/common';
import { ClientKafka } from '@nestjs/microservices';
import { PostCreatDto } from './dtos/postcreate';
@Injectable()
export class PostsService {
  constructor(
    @Inject('BLOG_SERVICE') private readonly blogService: ClientKafka,
  ) {}
  async createPost(data: PostCreatDto) {
    console.log('createPost');
    const postObj = {
      title: data.title,
      body: data.body,
      id: data.tokenData.id,
      authour: data.tokenData._id,
    };
    console.log(postObj);
    return this.promiseHandler('post.create', postObj);
  }
  async getPosts(req: any) {
    console.log('getPosts');
    console.log(req);
    const data = {
      id: req.tokenData.id,
    };
    return this.promiseHandler('post.list', data);
  }
  async getPostInfo(req: any, id: any) {
    console.log('getPosts');
    console.log(req);
    const data = {
      id: req.tokenData.id,
      postId: id,
    };
    return this.promiseHandler('post.info', data);
  }

  async promiseHandler(kafkaTopic: string, data: any) {
    return new Promise<any>((resolve, reject) =>
      this.blogService.send(kafkaTopic, data).subscribe({
        next: (data) => {
          console.log('data', data);
          resolve(data);
        },
        error: (error) => {
          console.log('error', error);
          reject({
            status: error.status || HttpStatus.BAD_REQUEST,
            message: error.message || 'System error!',
          });
        },
      }),
    );
  }
}
