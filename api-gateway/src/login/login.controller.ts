import { Body, Controller, HttpStatus, Post, Res } from '@nestjs/common';
import { LoginService } from './login.service';
@Controller('login')
export class LoginController {
  constructor(private LoginService: LoginService) {}
  @Post('')
  async login(@Body() req: any, @Res() res): Promise<any> {
    try {
      const result = await this.LoginService.login(req);
      res.status(HttpStatus.OK).send(result);
    } catch (err) {
      console.log(err);
      res.status(HttpStatus.OK).send(err);
    }
  }
}
