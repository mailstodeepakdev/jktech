import { Injectable } from '@nestjs/common';
import { AuthService } from '../auth/auth.service';
import { config } from '../config/configuration';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User } from '../mongodb/schema/user.schema';
@Injectable()
export class LoginService {
  constructor(
    private AuthService: AuthService,
    @InjectModel(User.name) private userModel: Model<User>,
  ) {}

  async login(user: any) {
    const loginData = {
      ...user,
      secret: config.JWTsecret,
      options: config.JWToptios,
    };
    const result = await this.AuthService.loginHandler(loginData);
    await this.userCreation(loginData);
    console.log(result);
    return result;
  }
  async userCreation(userObj: any) {
    try {
      const user = await this.userModel.findOne({ author: userObj.id });

      if (!user) {
        const userInfo = new this.userModel({ ...userObj });
        userInfo.save();
      }
    } catch (err) {
      console.log(err);
    }
  }
}
