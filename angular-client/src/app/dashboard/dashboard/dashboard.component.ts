import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})

export class DashboardComponent implements OnInit {
 
  constructor(private UserService:UserService){}

  public postData:any;
  ngOnInit(): void {
    this.getPostData();

  }
  getPostData(){
 
    this.UserService.getPublicPosts().subscribe((data:any) => {
      this.postData = data.result; 
    });

  }
  trackByFn(index: any, item: { id: any; }) {
    return item.id;
  }
}
