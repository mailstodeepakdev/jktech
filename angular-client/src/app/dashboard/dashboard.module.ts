import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module'; 
import { HeaderModule } from '../shared/header/header.module';
import { DashboardComponent } from './dashboard/dashboard.component'; 
import { PostModule } from '../post/post.module';
@NgModule({
  declarations: [ 
    DashboardComponent, 
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    HeaderModule,
    PostModule
  ]
})
export class DashboardModule { }
