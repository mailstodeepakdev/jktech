import { NgModule, inject } from '@angular/core';
import { RouterModule, Routes } from '@angular/router'; 
import { authGuard } from '../core/guards/auth.guard';
import { DashboardComponent } from './dashboard/dashboard.component'; 
const routes: Routes = [{
  path:'' , component:DashboardComponent, canActivate: [authGuard]
}, 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
