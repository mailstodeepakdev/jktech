export interface LoginModel {
    email: string;
    firstName: string;
    lastName: string;
    id: string;
}
