export interface PostCreateModel {
    title: string;
    body: string;
}
