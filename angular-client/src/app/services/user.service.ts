import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SharedService } from './shared.service'; 
import { LoginModel } from '../core/models/login-model';
import { PostCreateModel } from '../core/models/post-create.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private url: string;
  private token: string;
  private headers: HttpHeaders;

  constructor(private httpClient: HttpClient, private sharedService: SharedService) { 
    this.url = this.sharedService.getBaseUrl();
    this.token = this.sharedService.getToken();
    this.headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'token': this.token
    }); 
  }

  login(data: LoginModel) {
    const apiUrl = `${this.url}login`;
    return this.httpClient.post<any>(apiUrl, data);
  }

  createPost(data: PostCreateModel) {
    const apiUrl = `${this.url}posts`;
    return this.httpClient.post<any>(apiUrl, data, { headers: this.headers });
  }
  getPublicPosts() {
    const apiUrl = `${this.url}posts`;
    return this.httpClient.get<any>(apiUrl, { headers: this.headers });
  }
  getMyPosts() {
    const apiUrl = `${this.url}posts`;
    return this.httpClient.get<any>(apiUrl, { headers: this.headers });
  }
  getPostDetails(id: string){
    const apiUrl = `${this.url}posts/info?id=` + id;
    return this.httpClient.get<any>(apiUrl, { headers: this.headers });
  
  }
}
