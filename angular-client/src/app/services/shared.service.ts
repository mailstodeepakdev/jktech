import { EventEmitter, Injectable } from '@angular/core'; 
import {HttpHeaders } from '@angular/common/http'; 


@Injectable({
  providedIn: 'root'
})
export class SharedService {
  baseAuthUrl = 'http://localhost:8081/' ; 

  constructor( ) { }
  
  getToken() {
    const token:any = sessionStorage.getItem('token');    
    console.log('The token' + token);
    return token;    
  }
  getBaseUrl(){
    return this.baseAuthUrl;
  }
  getHeaders(){    
    const headers = new HttpHeaders({    
    'token': this.getToken(),  
    'Content-Type': 'application/json'    
   });    
      return headers;    
   }
}
