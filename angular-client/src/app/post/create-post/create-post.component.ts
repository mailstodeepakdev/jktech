import { Component, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormControl,Validators  } from '@angular/forms';
import { PostCreateModel } from 'src/app/core/models/post-create.model';
import { UserService } from '../../services/user.service';
@Component({
  selector: 'app-create-post',
  templateUrl: './create-post.component.html',
  styleUrls: ['./create-post.component.scss']
})
export class CreatePostComponent {

  @Output() postCreated = new EventEmitter();
  constructor(private UserService:UserService){}
  profileForm = new FormGroup({
    title: new FormControl('',Validators.required),
    body: new FormControl('', Validators.required),
  });
 
  createPost(){
    console.log(this.profileForm.value);
    const post: PostCreateModel = {
      title: this.profileForm.value.title,
      body: this.profileForm.value.body
    };
    this.UserService.createPost(post).subscribe((data) => {
      console.log(data);
      this.postCreated.emit(true);
    });

  }
}
