import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
@Component({
  selector: 'app-public-posts',
  templateUrl: './public-posts.component.html',
  styleUrls: ['./public-posts.component.scss']
})
export class PublicPostsComponent implements OnInit {
  private postList:any =[]
  constructor(private userService:UserService){

  }
  ngOnInit(): void {
    this.userService.getPublicPosts().subscribe((data) => {
      console.log(data);
      this.postList = data;
    });
  }

}
