import { Component, EventEmitter, Output, ViewChild } from '@angular/core';
import { MyPostsComponent } from '../my-posts/my-posts.component';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent {
 
 
  
@ViewChild(MyPostsComponent)
myPostsComponent: MyPostsComponent;

  postCreated(data: any){
    console.log('post created');
    console.log(data);
    this.myPostsComponent.ngOnInit();

  }
}
