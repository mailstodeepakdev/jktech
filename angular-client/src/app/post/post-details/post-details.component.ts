import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../../services/user.service';
@Component({
  selector: 'app-post-details',
  templateUrl: './post-details.component.html',
  styleUrls: ['./post-details.component.scss']
})
export class PostDetailsComponent implements OnInit{
  public title: string;
  public body: string;
  public postData: any =[];
  constructor(private route: ActivatedRoute , private UserService:UserService) { }
  ngOnInit() {
    const postId = this.route.snapshot.paramMap.get('id');
    console.log(postId);
    this.UserService.getPostDetails(postId).subscribe((data:any) => {
      console.log(data);
      if(data.result){
        this.title = data.result.title;
        this.body = data.result.body;
        this.postData = data.result;
      }
    });
  }
}
