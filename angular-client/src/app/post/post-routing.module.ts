import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { authGuard } from '../core/guards/auth.guard';
import { PostComponent } from './post/post.component';
import { PostDetailsComponent } from './post-details/post-details.component';
const routes: Routes = [
  {
    path: '', component: PostComponent,canActivate: [authGuard],
  },
  {
    path: 'create', component: PostComponent,canActivate: [authGuard],
  },
  {
    path: 'details/:id', component: PostDetailsComponent,canActivate: [authGuard],
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PostRoutingModule { }
