import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-my-posts',
  templateUrl: './my-posts.component.html',
  styleUrls: ['./my-posts.component.scss']
})
export class MyPostsComponent implements OnInit {
  public postList:any =[]
  constructor(private userService:UserService){}
 
  ngOnInit()  {
    this.userService.getMyPosts().subscribe((data) => {
      console.log(data);
      this.postList = data.result;
    });
  }
  
  trackByFn(index: any, item: { id: any; }) {
    return item.id;
  }
  }

