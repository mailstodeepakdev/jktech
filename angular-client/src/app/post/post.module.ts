import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PostRoutingModule } from './post-routing.module';
import { CreatePostComponent } from './create-post/create-post.component';
import { HeaderModule } from '../shared/header/header.module'; 
import { PostComponent } from './post/post.component';
import { PostBodyComponent } from './post-body/post-body.component'; 
import { ReactiveFormsModule } from '@angular/forms';
import { PublicPostsComponent } from './public-posts/public-posts.component';
import { MyPostsComponent } from './my-posts/my-posts.component';
import { PostDetailsComponent } from './post-details/post-details.component';

@NgModule({
  declarations: [
    CreatePostComponent, 
    PostComponent,
    PostBodyComponent,
    PublicPostsComponent,
    MyPostsComponent,
    PostDetailsComponent
  ],
  imports: [
    CommonModule,
    PostRoutingModule,
    HeaderModule,
    ReactiveFormsModule
  ],
  exports: [PostBodyComponent],
})
export class PostModule { }
