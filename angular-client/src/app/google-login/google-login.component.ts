
import { CommonModule } from '@angular/common'; 
import { GoogleLoginProvider, SocialAuthService, SocialAuthServiceConfig, SocialUser ,GoogleSigninButtonModule , SocialLoginModule} from '@abacritt/angularx-social-login';
import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router'; 
import { HttpClientModule } from '@angular/common/http';

@Component({
  selector: 'app-google-login',
  standalone: true,
  imports: [CommonModule,
    HttpClientModule,
    SocialLoginModule,
    GoogleSigninButtonModule],
  templateUrl: './google-login.component.html',
  styleUrls: ['./google-login.component.scss'],
  
  providers: [
    {
      provide: 'SocialAuthServiceConfig',
      useValue: {
        autoLogin: false,
        providers: [
          {
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider(
              '663990587528-hmu2scpu3t5m3eablhkmb62uq57r1i8c.apps.googleusercontent.com'
            )
          }
        ],
        onError: (err: any) => {
          console.error(err);
        }
      } as SocialAuthServiceConfig,
    }
  ],
})
export class GoogleLoginComponent implements OnInit{

  user: SocialUser;
  loggedIn: boolean;
  
  constructor(private authService: SocialAuthService , private UserService:UserService , private router: Router) { }

  ngOnInit() {
    sessionStorage.removeItem("token");
    this.authService.authState.subscribe((user) => {
      this.user = user;
      this.loggedIn = (user != null);
      console.log(this.user);
      if(user.email){
        this.UserService.login(user).subscribe((data: any) => {
          console.log(data);
          if(!data.token){
            alert('Error! Please try again');
          }else{
            sessionStorage.setItem('token', data.token);
            const token = sessionStorage.getItem('token'); 
            this.router.navigate(['/dashboard']);

          }

        })
      }
    });
  }
}
