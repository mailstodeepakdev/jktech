import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
const routes: Routes = [ 
  { path: '', loadComponent: () => import('./google-login/google-login.component').then((m) => m.GoogleLoginComponent) },
  { path: 'login', loadComponent: () => import('./google-login/google-login.component').then((m) => m.GoogleLoginComponent) },
  { path: 'dashboard', loadChildren: () => import('./dashboard/dashboard.module').then((m) => m.DashboardModule) },
  { path: 'post', loadChildren: () => import('./post/post.module').then((m) => m.PostModule) },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
