# jktech



## Activities completed
- [.] Login with google 
- [.] Post listing 
- [.] Post creation
- [.] Post view details
- [.] Log out


## Backend 
- [.] Implimented Authentication using JWT
- [.] Implimented api-gateway micro service pattern
- [.] Used Kafka to communicate between the micro services
- [.] Used mongoDB to store data
- [.] Used Mongoose for the database operations

## Frontend 
- [.] Used Angualr 15
- [.] Implimented Bootstrap4
- [.] Used Guards
- [.] Used Angular google package to login

## Application start up flow

```
cd jk
docker compose up -d
cd api-gateway
npm i
npm run start:dev
```

```
cd ..
cd blogs
npm i
npm run start:dev
```

```
cd ..
cd angular-client
npm i
ng serve
```

## TODO
 - [.] Api Documentation
 - [.] More CSS styling for the Frontend
 - [.] Database indexing and reffering schemas
 - [.] Code optimization
 - [.] Terraform code completion
 - [.] AWS side activities
 - [.] Ngrx usage
 - [.] Completion of test cases etc