terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.16"
    }
  }

  required_version = ">= 1.2.0"
}

provider "aws" {
  region = "ap-south-1"
}

resource "aws_instance" "csms_charger" {
  ami           = "ami-0f5ee92e2d63afc18"
  instance_type = "t2.micro"

  tags = {
    Name = "csms charger"
  }

  user_data = <<-EOF
              #!/bin/bash
              apt update -y
              curl -sL https://deb.nodesource.com/setup_16.x | sudo -E bash -
              apt install -y nodejs
              npm install -g npm@latest pm2 -y
              cd /
              git clone  https://mailstodeepakdev:glpat-csrVnNKWtYk8ztxo_Mng@gitlab.com/mailstodeepakdev/jktech.git
              cd amrc
              npm install
              npm run build
              pm2 start npm -- start
              EOF
}

resource "aws_security_group" "csms_charger_sg" {
  name_prefix = "csms_charger_sg_"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "csms_charger_sg"
  }
}

resource "aws_security_group_rule" "egress_rule" {
  security_group_id = aws_security_group.csms_charger_sg.id

  type        = "egress"
  from_port   = 0
  to_port     = 0
  protocol    = "-1"
  cidr_blocks = ["0.0.0.0/0"]
} 