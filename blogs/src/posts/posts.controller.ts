import { Controller, HttpStatus } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { PostsService } from './posts.service';
@Controller('')
export class PostsController {
  constructor(private PostsService: PostsService) {}
  @MessagePattern('post.create')
  async postCreation(@Payload() data: any) {
    console.log(data);
    try {
      const result = await this.PostsService.createPost(data);
      return {
        status: HttpStatus.OK,
        result,
      };
    } catch (err) {
      console.log(err);
      return {
        status: HttpStatus.NOT_ACCEPTABLE,
        message: err.toString() || 'System error!',
      };
    }
  }
  @MessagePattern('post.list')
  async postList(@Payload() data: any) {
    console.log(data);
    try {
      const result = await this.PostsService.listPost(data);
      return {
        status: HttpStatus.OK,
        result,
      };
    } catch (err) {
      console.log(err);
      return {
        status: HttpStatus.NOT_ACCEPTABLE,
        message: err.toString() || 'System error!',
      };
    }
  }
  @MessagePattern('post.info')
  async postInfo(@Payload() data: any) {
    console.log(data);
    try {
      const result = await this.PostsService.postInfo(data);
      return {
        status: HttpStatus.OK,
        result,
      };
    } catch (err) {
      console.log(err);
      return {
        status: HttpStatus.NOT_ACCEPTABLE,
        message: err.toString() || 'System error!',
      };
    }
  }
}
