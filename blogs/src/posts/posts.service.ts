import { Injectable } from '@nestjs/common';
import { Post } from '../mongodb/schema/post.schema';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
@Injectable()
export class PostsService {
  constructor(@InjectModel(Post.name) private postModel: Model<Post>) {}

  async createPost(data: any) {
    console.log('creating post');
    console.log(data);
    const postInfo = new this.postModel({ ...data });
    postInfo.save();
    return true;
  }
  async listPost(data: any) {
    console.log('creating post');
    console.log(data);
    const posts = this.postModel.find({ id: data.id }).sort({ createdAt: -1 });

    return posts;
  }
  async postInfo(data: any) {
    console.log('creating post');
    console.log(data);
    const posts = this.postModel.findOne({ id: data.id, _id: data.postId });

    return posts;
  }
}
