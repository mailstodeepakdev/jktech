import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { PostsModule } from './posts/posts.module';
import { MongodbModule } from './mongodb/mongodb.module';

@Module({
  imports: [PostsModule, MongodbModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
